package fr.ubordeaux.ao;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Ellipse extends Circle {
    private final int radiusY;

    public Ellipse(int centerX, int centerY, int radiusX, int radiusY) {
        super(centerX, centerY, radiusX);
        this.radiusY = radiusY;
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        java.awt.Shape s;
        graphics2D.draw(new Ellipse2D.Double(this.x, this.y, this.radiusX, this.radiusY));
    }
}
package fr.ubordeaux.ao;

import java.awt.*;

public class Line extends Shape {
    private final int x2;
    private final int y2;

    public Line(int x1, int y1, int x2, int y2) {
        super(x1, y1);
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        graphics2D.drawLine(this.x, this.y, this.x2, this.y2);
    }
}
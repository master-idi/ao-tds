package fr.ubordeaux.ao;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Circle extends Shape {
    protected int radiusX;

    public Circle(int centerX, int centerY, int radius) {
        super(centerX, centerY);
        this.radiusX = radius;
    }

    @Override
    public void draw(Graphics2D graphics2D) {
        graphics2D.draw(new Ellipse2D.Double(this.x, this.y, this.radiusX, this.radiusX));
    }
}
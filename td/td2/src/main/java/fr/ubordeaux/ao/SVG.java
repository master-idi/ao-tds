package fr.ubordeaux.ao;

import org.jfree.svg.SVGGraphics2D;
import org.jfree.svg.SVGUtils;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SVG {
    private final int width;
    private final int height;
    private final ArrayList<Shape> shapesList;

    private final String filename = "shapes.svg";

    public SVG(int width, int height, ArrayList<Shape> shapesList) {
        this.width = width;
        this.height = height;
        this.shapesList = shapesList;
    }

    public void generateSVG() {
        SVGGraphics2D graphics2D = new SVGGraphics2D(this.width, this.height);
        graphics2D.setPaint(new Color(58, 126, 107));

        if (this.shapesList.size() < 1) {
            return;
        }

        this.shapesList.forEach(shape -> {
            shape.draw(graphics2D);
        });

        this.createSVGFile(graphics2D);
    }

    private void createSVGFile(SVGGraphics2D graphics2D) {
        try {
            SVGUtils.writeToSVG(new File(filename), graphics2D.getSVGElement());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
package fr.ubordeaux.ao;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

  public static void main(String[] args) throws IOException {
    ArrayList<Shape> shapesList = new ArrayList<Shape>();

    shapesList.add(new Rectangle(13, 20, 200, 100));
    shapesList.add(new Line(50, 300, 200, 130));
    shapesList.add(new Circle(300, 200, 50));
    shapesList.add(new Ellipse(500, 200, 80, 50));

    SVG svg = new SVG(600, 600, shapesList);
    svg.generateSVG();
  }
}
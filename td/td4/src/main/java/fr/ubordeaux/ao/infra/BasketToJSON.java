package fr.ubordeaux.ao.infra;

import com.google.gson.Gson;
import fr.ubordeaux.ao.domain.aggregate.Basket;
import fr.ubordeaux.ao.domain.repository.BasketRepository;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class BasketToJSON implements BasketRepository {

    private Gson gson;

    public BasketToJSON() {
        this.gson = new Gson();
    }

    @Override
    public void save(Basket basket) {
        try {
            String serializedBasket = this.gson.toJson(basket);
            FileWriter jsonFile = new FileWriter(Basket.class.getSimpleName().toLowerCase() + ".json");
            jsonFile.write(serializedBasket);
            jsonFile.close();
        } catch (IOException e) {
            System.out.println("Unable to create file.");
            e.printStackTrace();
        }
    }

    @Override
    public Basket load() {
        try {
            String serializedJson = "";
            File jsonFile = new File(Basket.class.getSimpleName().toLowerCase() + ".json");
            Scanner reader = new Scanner(jsonFile);
            while (reader.hasNextLine()) {
                serializedJson = serializedJson.concat(reader.nextLine());
            }
            reader.close();
            return this.gson.fromJson(serializedJson, Basket.class);
        } catch (IOException e) {
            System.out.println("Unable to create file.");
            e.printStackTrace();
            return null;
        }
    }
}

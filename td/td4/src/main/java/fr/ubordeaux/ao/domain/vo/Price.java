package fr.ubordeaux.ao.domain.vo;

import java.util.Objects;

public class Price {

    private final double value;

    public Price(double value) {
        this.checkPriceValidity(value);
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }

    public double getDiscount(double discount) {
        return this.value - discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return value == price.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    private void checkPriceValidity(double value) {
        if (value < 0) {
            throw new IllegalArgumentException();
        }
    }
}

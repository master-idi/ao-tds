package fr.ubordeaux.ao.domain.aggregate;

import fr.ubordeaux.ao.domain.entity.Order;
import fr.ubordeaux.ao.domain.vo.Price;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Basket {

    private final HashMap<UUID, Order> orders = new HashMap<>();
    private final Price price;
    private boolean isValidated = false;

    public Basket(ArrayList<Order> orders) {
        orders.forEach(this::addOrder);
        double amount = orders.stream().mapToDouble(order -> order.getAmount().getValue()).sum();
        this.price = new Price(amount);
    }

    public Price getPrice() {
        return this.price;
    }

    public void validate() {
        this.isValidated = true;
    }

    public void addOrder(Order order) {
        if (!this.isValidated && !this.isOrderProductInBasket(order)) {
            this.orders.put(order.getProduct().getReference(), order);
        }
    }

    public void removeOrderByRef(UUID reference) {
        if (!this.isValidated) {
            Order order = this.orders.get(reference);
            if (order != null) {
                this.orders.remove(reference);
            }
        }
    }

    private boolean isOrderProductInBasket(Order order) {
        return this.orders.containsKey(order.getProduct().getReference());
    }
}

package fr.ubordeaux.ao.domain.entity;

import fr.ubordeaux.ao.domain.vo.Price;
import fr.ubordeaux.ao.domain.vo.Product;

import java.util.Objects;

public class Order {

    private final Product product;
    private final double quantity;
    private final Price amount;

    public Order(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
        this.amount = new Price(product.getPrice().getValue() * quantity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Double.compare(order.quantity, quantity) == 0 &&
                product.equals(order.product) &&
                amount.equals(order.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, quantity, amount);
    }

    public Product getProduct() {
        return this.product;
    }

    public double getQuantity() {
        return this.quantity;
    }

    public Price getAmount() {
        return this.amount;
    }
}

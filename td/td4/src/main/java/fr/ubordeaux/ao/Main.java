package fr.ubordeaux.ao;

import fr.ubordeaux.ao.domain.aggregate.Basket;
import fr.ubordeaux.ao.domain.entity.Order;
import fr.ubordeaux.ao.domain.repository.BasketRepository;
import fr.ubordeaux.ao.domain.vo.Price;
import fr.ubordeaux.ao.domain.vo.Product;
import fr.ubordeaux.ao.infra.BasketToJSON;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Order> orders = new ArrayList<>();

        Product product = new Product("Lait", "Produit laitier", new Price(3.50));
        Order order1 = new Order(product, 2);
        orders.add(order1);

        Basket basket = new Basket(orders);

        BasketRepository basketRepository = new BasketToJSON();
        basket.validate();
        basketRepository.save(basket);
    }
}

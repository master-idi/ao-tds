package fr.ubordeaux.ao.domain.vo;

import java.util.Objects;
import java.util.UUID;

public class Product {

    private final UUID reference;
    private final String name;
    private final String description;
    private final Price price;

    public Product(String name, String description, Price price) {
        this.reference = UUID.randomUUID();
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public UUID getReference() {
        return this.reference;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public Price getPrice() {
        return this.price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return reference.equals(product.reference) &&
                name.equals(product.name) &&
                description.equals(product.description) &&
                price.equals(product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference, name, description, price);
    }
}

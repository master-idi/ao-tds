package fr.ubordeaux.ao.domain.repository;

import fr.ubordeaux.ao.domain.aggregate.Basket;

public interface BasketRepository {

    void save(Basket basket);
    Basket load();

}

package fr.ubordeaux.ao.infra;

import fr.ubordeaux.ao.domain.aggregate.Basket;
import fr.ubordeaux.ao.domain.repository.BasketRepository;

public class BasketInMemory implements BasketRepository {

    private Basket basket;

    public BasketInMemory() {

    }

    @Override
    public void save(Basket basket) {
        this.basket = basket;
    }

    @Override
    public Basket load() {
        return this.basket;
    }
}

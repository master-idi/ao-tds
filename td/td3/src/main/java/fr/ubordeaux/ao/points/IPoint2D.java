package fr.ubordeaux.ao.points;

public interface IPoint2D {
    public int getX();
    public int getY();
}
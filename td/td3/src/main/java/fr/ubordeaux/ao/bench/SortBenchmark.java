package fr.ubordeaux.ao.bench;

import java.util.ArrayList;

public class SortBenchmark {
    private final ArrayList<Sort> sortAlgos = new ArrayList<>();

    public SortBenchmark() {

    }

    public void addAlgorithm(Sort sortAlgo) {
        this.sortAlgos.add(sortAlgo);
    }

    public void bench(String[] unsorted) {
        this.sortAlgos.forEach(algo -> {
            Long timeBefore = System.currentTimeMillis();
            algo.sort(unsorted);
            Long timeAfter = System.currentTimeMillis();
            System.out.println("*===============");
            System.out.println("Execution time of " + algo.getName() + " : " + (timeAfter - timeBefore));
            System.out.println("*===============");
        });
    }
}
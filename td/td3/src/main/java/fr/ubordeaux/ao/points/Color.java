package fr.ubordeaux.ao.points;

public class Color implements IColor {
    private int r;
    private int g;
    private int b;

    public Color(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    @Override
    public int getGreen() {
        return 0;
    }
}
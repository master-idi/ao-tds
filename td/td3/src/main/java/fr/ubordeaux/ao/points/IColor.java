package fr.ubordeaux.ao.points;

public interface IColor {
    public int getGreen();
}
package fr.ubordeaux.ao.bench;

public class InsertTri implements Sort {
    private static InsertTri mInstance = null;
    private String name;


    private InsertTri() {
        this.name = "Insert";
    }

    public static InsertTri getInstance()
    {
        if(mInstance == null) {
            mInstance = new InsertTri();
        }

        return mInstance;
    }

    @Override
    public String[] sort(String[] unsorted) {
        String tmp = "";
        String[] sorted = unsorted.clone();
        int j;

        for (int i = 1; i < sorted.length; i++) {
            tmp = sorted[i];
            j = i;
            while (j > 0 && sorted[j-1].compareTo(tmp) > 0) {
                sorted[j] = sorted[j-1];
                j = j-1;
            }
            sorted[j] = tmp;
        }
        return sorted;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
package fr.ubordeaux.ao.bench;

public interface Sort {
    String[] sort(String[] unsorted);
    String getName();
}

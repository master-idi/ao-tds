package fr.ubordeaux.ao.points;

public class ColoredPoint2D implements IColor, IPoint2D {
    private Point2D point2D;
    private Color color;
    private int g;

    public ColoredPoint2D(Point2D point2D, Color color, int g) {
        this.point2D = point2D;
        this.color = color;
        this.g = g;
    }

    @Override
    public int getGreen() {
        return this.g;
    }

    @Override
    public int getX() {
        return this.point2D.getX();
    }

    @Override
    public int getY() {
        return this.point2D.getY();
    }
}

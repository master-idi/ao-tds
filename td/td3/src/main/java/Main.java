import fr.ubordeaux.ao.bench.InsertTri;
import fr.ubordeaux.ao.bench.SortBenchmark;

public class Main {

    public static void main(String[] args) {
        String[] unsorted = new String[]{
                "FEUHU",
                "PSKZNKF",
                "PERIUNC",
                "MAPRUBCHDSKSJ",
                "NCPAPSURBBC",
                "SBNF",
                "NCOSNF"
        };
        SortBenchmark bench = new SortBenchmark();
        bench.addAlgorithm(InsertTri.getInstance());
        bench.bench(unsorted);
    }
}
